import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import * as ROUTES from '../../constants/routes';
import { withFirebase } from '../firebase';
import { compose } from 'recompose';
import * as ROLES from '../../constants/roles';
import Footer from '../footer'; 

const SignUpPage = () => (
    <div>
    <SignUpForm />
    <Footer/>

  </div>
);


const INITIAL_STATE = {
    username: '',
    email: '',
    passwordOne: '',
    passwordTwo: '',
    isAdmin: false,

    error: null,
  };

  class SignUpFormBase extends Component {
    constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };

  }
  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  
  onChangeCheckbox = event => {
    this.setState({ [event.target.name]: event.target.checked });
  };

  onSubmit = event => {
    const { username, email, passwordOne, isAdmin } = this.state;
    const roles = {};
    if (isAdmin) {
      roles[ROLES.ADMIN] = ROLES.ADMIN;
    }
    this.props.firebase
    .doCreateUserWithEmailAndPassword(email, passwordOne)
    .then(authUser => {
      // Create a user in your Firebase realtime database
      return this.props.firebase
        .user(authUser.user.uid)
        .set({
          username,
          email,
          roles,

        });
    })
    .then(() => {
      this.setState({ ...INITIAL_STATE });
      this.props.history.push(ROUTES.HOME);
    })
    .catch(error => {
      this.setState({ error });
    });
  event.preventDefault();
};


  render() {
    const {
      username,
      email,
      passwordOne,
      passwordTwo,
      isAdmin,
      error,
    } = this.state;
    const isInvalid =
      passwordOne !== passwordTwo ||
      passwordOne === '' ||
      email === '' ||
      username === '';
    return (
      <section class="space-md border-bottom bg-light">
      <div class="mask bg-dark"></div> 
      <div class="container">
            <div class="row d-flex justify-content-center text-center">
                <div class="col-lg-8">
                <div class="w-85">
                        <h1> Sign Up  </h1>
                        <p class="lead">Pour découvrir au quotidien de nouveaux livres ! Gardez toujours un oeil sur BookApplication.com</p>
                        
      <form onSubmit={this.onSubmit} class="form-signin">
      <div class="row row d-flex justify-content-center mt-5">
          <div class="col-md-8">

          <div class="form-message mb-3">

<div class="input-group input-group form">

   <div class="input-group-prepend form__prepend"> 

     <span class="input-group-text form__text">
      <span class="fa fa-user form__text-inner"></span> 
      </span>
    </div>
    <input
          name="username"
          value={username}
          onChange={this.onChange}
          type="text"
          placeholder="Full Name"
          class="form-control"
        />
        </div>
</div>


             <div class="form-message mb-3">

                <div class="input-group input-group form">
         

                   <div class="input-group-prepend form__prepend"> 

                     <span class="input-group-text form__text">
                      <span class="fa fa-envelope form__text-inner"></span> 
                      </span>
                    </div>
                    <input name="email" class="form-control"  value={email} onChange={this.onChange} type="text" placeholder="Email Address"/>
               </div>
       </div>
       <div class="form-message mb-3">

<div class="input-group input-group form">

   <div class="input-group-prepend form__prepend"> 

     <span class="input-group-text form__text">
      <span class="fa fa-lock form__text-inner"></span> 
      </span>
    </div>
    <input
          name="passwordOne"
          value={passwordOne}
          onChange={this.onChange}
          type="password"
          placeholder="Password"
          class="form-control"

        />
        </div>
</div>
<div class="form-message mb-3">

<div class="input-group input-group form">

   <div class="input-group-prepend form__prepend"> 

     <span class="input-group-text form__text">
      <span class="fa fa-unlock form__text-inner"></span> 
      </span>
    </div>
    <input
          name="passwordTwo"
          value={passwordTwo}
          onChange={this.onChange}
          type="password"
          placeholder="Confirm Password"
          class="form-control"
          />
        </div>
</div>
      
          <label>
          Vous êtes Administrateur ? :
            <input
              name="isAdmin"
              type="checkbox"
              checked={isAdmin}
              onChange={this.onChangeCheckbox}
            />
          </label>
          <button disabled={isInvalid} type="submit"class="btn btn-block btn-gradient">
        Sign Up
      </button>
      {error && <p>{error.message}</p>}
      </div></div>
    </form>
    </div>
    </div></div>
</div>  
 </section>
    );
  }
}
const SignUpLink = () => (
  <p>
  Inscrivez vous ! <Link to={ROUTES.SIGN_UP}>Sign Up</Link>
  </p>
);
//const SignUpForm = withRouter(withFirebase(SignUpFormBase));
const SignUpForm = compose(
  withRouter,
  withFirebase,
)(SignUpFormBase);
export default SignUpPage;
export { SignUpForm , SignUpLink } ; 
