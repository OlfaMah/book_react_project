import React from 'react';
import { AuthUserContext, withAuthorization } from '../Session';
import { PasswordForgetForm } from '../PasswordForget';
import PasswordChangeForm from '../PasswordChange';
import Footer from '../footer';

const AccountPage = () => (
  <AuthUserContext.Consumer>
  {authUser => (
    <div class="container">
      <h2>Votre E-mail: {authUser.email}</h2>
      <p>Si vous avez oublier votre mot de passe !</p>
      <PasswordForgetForm />
      <p>Si vous voulez changer votre mot de passe</p>
      <PasswordChangeForm />
      <Footer />

    </div>
  )}

</AuthUserContext.Consumer>
);
const condition = authUser => !!authUser;
export default withAuthorization(condition)(AccountPage);