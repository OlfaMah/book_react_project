import React, { Component } from 'react';

import { withFirebase } from '../firebase';

class UserItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      user: null,
      ...props.location.state,
    };
  }

  componentDidMount() {
    if (this.state.user) {
      return;
    }

    this.setState({ loading: true });

    this.props.firebase
      .user(this.props.match.params.id)
      .on('value', snapshot => {
        this.setState({
          user: snapshot.val(),
          loading: false,
        });
      });
  }

  componentWillUnmount() {
    this.props.firebase.user(this.props.match.params.id).off();
  }

  onSendPasswordResetEmail = () => {
    this.props.firebase.doPasswordReset(this.state.user.email);
  };

  render() {
    const { user, loading } = this.state;

    return (
      <div class="container">
        <h5> Utilisateur ({this.props.match.params.id})</h5>
        {loading && <div>Loading ...</div>}

      
            <div class="table-responsive">
           <table class="table">
       <thead>
       <tr>
       <th scope="col"> ID </th>
       <th scope="col"> E-Mail </th>
       <th scope="col"> Username </th>
       <th scope="col">Update</th>
       </tr>
       </thead>
       <tbody>
       {user && (
       <tr>
       <td>{user.uid}</td>
       <td>{user.email}</td>
       <td>{user.username}</td>
       <td><button
                       type="button"
                       class="btn btn-outline-primary"
                       onClick={this.onSendPasswordResetEmail}
                     >
                       Send Password Reset
                     </button></td>
       
       </tr>
       )}
       </tbody>
       </table></div>
       }
       
      </div>
    );
  }
}

export default withFirebase(UserItem);
