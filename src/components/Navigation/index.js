import React from 'react';
import { Link } from 'react-router-dom';
import SignOutButton from '../SignOut';
import * as ROUTES from '../../constants/routes';
import { AuthUserContext } from '../Session';

const Navigation = () => (
  <div>
    <AuthUserContext.Consumer>
      {authUser =>
        authUser ? <NavigationAuth /> : <NavigationNonAuth />
      }
    </AuthUserContext.Consumer>
  </div>
);
const NavigationAuth = () => (
  <nav class="navbar navbar-expand-lg navbar-light">
  <div class="container">
  
<a class="navbar-brand js-scroll-trigger" href="#page-top">BooksApplication.com</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>

<div class="collapse navbar-collapse" id="navbarResponsive">
<ul class="navbar-nav ml-auto">
    <li class="nav-item "> <Link to={ROUTES.LANDING}class="nav-link js-scroll-trigger"  >Home </Link></li> 
    <li class="nav-item ">  <Link to={ROUTES.HOME}class="nav-link js-scroll-trigger"> Search Books  </Link></li>
    <li class="nav-item ">  <Link  to={ROUTES.ACCOUNT}class="nav-link js-scroll-trigger" >Compte</Link></li>
    <li class="nav-item ">  <Link  to={ROUTES.ADMIN}class="nav-link js-scroll-trigger" >Espace Administrateur</Link></li>
      <SignOutButton/> 

    </ul>
  </div>
  </div>
</nav>
);
const NavigationNonAuth = () => (
<nav class="navbar navbar-expand-lg navbar-light">
  <div class="container">
  
<a class="navbar-brand js-scroll-trigger" href="#page-top">BooksApplication.com</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>

<div class="collapse navbar-collapse" id="navbarResponsive">
<ul class="navbar-nav ml-auto">
    <li class="nav-item "> <Link to={ROUTES.LANDING}class="nav-link js-scroll-trigger"  >Home </Link></li> 
  <li class="nav-item ">
  <button  class="btn btn btn-lg border ml-4" >
      <Link to={ROUTES.SIGN_IN}>Sign In</Link>
    </button></li>
    </ul>
  </div>
  </div>
</nav>
);
export default Navigation;