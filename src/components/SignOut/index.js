import React from 'react';
import { withFirebase } from '../firebase';
const SignOutButton = ({ firebase }) => (
  <button type="button" class="btn btn btn-lg btn-gradient border ml-4"  onClick={firebase.doSignOut}>
    Sign Out
  </button>
);
export default withFirebase(SignOutButton);