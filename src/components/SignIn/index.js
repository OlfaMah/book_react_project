import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { SignUpLink } from '../SignUp';
import { withFirebase } from '../firebase';
import * as ROUTES from '../../constants/routes';
import { PasswordForgetLink } from '../PasswordForget';
import Footer from '../footer';

const SignInPage = () => (
  
    <div class="">
    <SignInForm />
    <Footer/>

    </div>
);

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
};

const ERROR_CODE_ACCOUNT_EXISTS =
  'auth/account-exists-with-different-credential';

const ERROR_MSG_ACCOUNT_EXISTS = `
  An account with an E-Mail address to
  this social account already exists. Try to login from
  this account instead and associate your social accounts on
  your personal account page.
`;

class SignInFormBase extends Component {
  constructor(props) {
    super(props);
    this.state = { ...INITIAL_STATE };
  }
  onSubmit = event => {
    const { email, password } = this.state;
    this.props.firebase
      .doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState({ ...INITIAL_STATE });
        this.props.history.push(ROUTES.HOME);
      })
      .catch(error => {
        this.setState({ error });
      });
    event.preventDefault();
  };
  onChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };
  render() {
    const { email, password, error } = this.state;
    const isInvalid = password === '' || email === '';
    return (
      <section class="space-md border-bottom bg-light" id="signup">
 <div class="mask bg-dark"></div>
 <div class="container">
            <div class="row d-flex justify-content-center text-center">
                <div class="col-lg-8">
                <div class="w-85">
                        <h1> Sign In  </h1>
                        <p class="lead">Pour découvrir au quotidien de nouveaux livres ! Gardez toujours un oeil sur BookApplication.com</p>
                        
       <form onSubmit={this.onSubmit} class="form-signin">
       <div class="row row d-flex justify-content-center mt-5">
                            <div class="col-md-8">
                            <div class="form-message mb-3">
                                    <div class="input-group input-group form">
                                        <div class="input-group-prepend form__prepend"> <span class="input-group-text form__text">
                                                <span class="fa fa-envelope form__text-inner"></span> </span>
                                        </div>
                                        <input type="text" class="form-control" name="email"
          value={email}  placeholder="Enter your email address"  onChange={this.onChange} ></input>
                                </div>
       </div>
       <div class="form-message mb-3">
                                    <div class="input-group input-group form">
                                        <div class="input-group-prepend form__prepend"> <span class="input-group-text form__text">
                                                <span class="fa fa-lock form__text-inner"></span> </span>
                                        </div>
                                        <input type="password" class="form-control form__input"  name="password"
          value={password}
          onChange={this.onChange} required="" placeholder="Create your password" aria-label="Create your password"></input>
                                    </div>
                                </div>
    
        <button disabled={isInvalid} type="submit"class="btn btn-block btn-gradient">
          Sign In
        </button>
        {error && <p>{error.message}</p>}
        <div class="btn-group" role="group" aria-label="...">
        
        <SignInGoogle/>
        <SignInFacebook/>
        </div>
        < PasswordForgetLink /> 
    <SignUpLink /> 
      </div></div>
      </form>
      </div>
      </div>
      </div>
      </div>
      
      </section>
      
    );
  }
}
class SignInGoogleBase extends Component {
  constructor(props) {
    super(props);

    this.state = { error: null };
  }

  onSubmit = event => {
    this.props.firebase
      .doSignInWithGoogle()
      .then(socialAuthUser => {
        // Create a user in your Firebase Realtime Database too
        return this.props.firebase.user(socialAuthUser.user.uid).set({
          username: socialAuthUser.user.displayName,
          email: socialAuthUser.user.email,
          roles: {},
        });
      })
      .then(() => {
        this.setState({ error: null });
        this.props.history.push(ROUTES.HOME);
      })
      .catch(error => {
        if (error.code === ERROR_CODE_ACCOUNT_EXISTS) {
          error.message = ERROR_MSG_ACCOUNT_EXISTS;
        }

        this.setState({ error });
      });

    event.preventDefault();
  };

  render() {
    const { error } = this.state;

    return (
      <form onSubmit={this.onSubmit}>
  
        <button type="submit" class="btn btn-block btn-lg" ><li class="fa fa-google"> Sign In with Google</li></button>

        {error && <p>{error.message}</p>}
       
      </form>
    );
  }
}

class SignInFacebookBase extends Component {
  constructor(props) {
    super(props);

    this.state = { error: null };
  }

  onSubmit = event => {
    this.props.firebase
      .doSignInWithFacebook()
      .then(socialAuthUser => {
        // Create a user in your Firebase Realtime Database too
        return this.props.firebase.user(socialAuthUser.user.uid).set({
          username: socialAuthUser.additionalUserInfo.profile.name,
          email: socialAuthUser.additionalUserInfo.profile.email,
          roles: {},
        });
      })
      .then(() => {
        this.setState({ error: null });
        this.props.history.push(ROUTES.HOME);
      })
      .catch(error => {
        if (error.code === ERROR_CODE_ACCOUNT_EXISTS) {
          error.message = ERROR_MSG_ACCOUNT_EXISTS;
        }

        this.setState({ error });
      });

    event.preventDefault();
  };

  render() {
    const { error } = this.state;

    return (
      <form onSubmit={this.onSubmit}>
    
        <button type="submit" class="btn btn-block btn-lg"><li class="fa fa-facebook"> Sign In with Facebook</li></button>

        {error && <p>{error.message}</p>}
      </form>
    );
  }
}

const SignInGoogle = compose(
  withRouter,  
  withFirebase,
)(SignInGoogleBase);

const SignInFacebook = compose(
  withRouter,
  withFirebase,
)(SignInFacebookBase);

const SignInForm = compose(
  withRouter,
  withFirebase,
)(SignInFormBase);
export default SignInPage;
export { SignInForm, SignInGoogle, SignInFacebook };
