import React, { Component } from 'react';
import Books from '../Book/Books';
import Footer from '../footer'
import './Home.css';

class HomePage extends Component {
  render() {
  return (
    <div>
    <section class="space-md border-bottom bg-light" >
    <div className="App">
     <Books />
    <Footer/>
    </div>
    </section>
         </div>
     );
  }
}
export default HomePage;
