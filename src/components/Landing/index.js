import React from 'react';
import Footer from '../footer';

const Landing = () => (
  <section class="hero space-md">
        <div class="container">
            <div class="row d-flex align-items-center justify-content-center text-center">
                <div class="col-md-10">
                    <div class="title-content-warpper">
                        <h1>Obtenez nos meilleures ressources de livres disponibles dans le marché !</h1>
                    </div>
                    <h4 class="mt-3 mb-3"> Rejoignez plus de 100 000 personnes qui reçoivent notre newsletter du BookApplication.com</h4>
                    <p><a href="#}" class="btn btn-lg btn-gradient">Signup for trial</a></p>
                    <p><small>🔥 1241 signed up for the demo in the last month </small></p>
                </div>
            </div>
      
        </div>
        <Footer/>
    </section>
);

export default Landing;
