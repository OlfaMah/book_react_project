import React from 'react';


const SearchArea =(props) => {
    return (
<div class="container">
        <div className="search-area">
     
        <form onSubmit={props.searchBook} action="">
        <div class="input-group">
        
  <input  onChange={props.handleSearch } type="text" class="form-control" placeholder="Books name" aria-label="Books names" aria-describedby="button-addon4"/>
  <div class="input-group-append" id="button-addon4">
    <button class="btn btn-outline-primary" type="submit">Search </button>
    <select   class="btn btn-outline-primary dropdown-toggle" defaultValue="Sort" onChange={props.handleSort}>
            <option class="dropdown-item" disabled value="Sort">Sort</option>
            <option class="dropdown-item" value="Newest">Newest</option>
            <option class="dropdown-item" value="Oldest">Oldest </option>

        </select>
  </div>
</div>
      
        </form>
        </div>   </div>
)
} 

    export default SearchArea;
