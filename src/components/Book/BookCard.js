import React from 'react';

const BookCard = (props) => {
    return(
        
<div class="card-deck">
            <div class="card"> <div class="card-body">
            <img src={props.image} class="img-thumbnail" alt=""></img>
                       
                        <h3 class="card-title">Titre {props.title}</h3>
                        <p class="card-text" >Auteur :{props.author}</p>
                        <p class="card-text">Date de publication :{props.published}</p>
                        </div>
            </div>
</div>
    )

}

export default BookCard;