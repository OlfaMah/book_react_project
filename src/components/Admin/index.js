import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { compose } from 'recompose';

import { withAuthorization } from '../Session';
import { UserList, UserItem } from '../Users';
import * as ROLES from '../../constants/roles';
import * as ROUTES from '../../constants/routes';
import Footer from '../footer';
const AdminPage = () => (
  <div>
     <div class="space-md" id="service">
        <div class="container">
            <div class="row d-flex justify-content-center mb-md-4 mb-sm-3">
                <div class="col-md-9 text-center">
                    <p class="-label">Espace de gestions des utilisateurs</p>
                    <h4 class="h1">Consulter tout les utilisateurs en tant que Administrateur</h4>
                </div>
            </div>
            <section>
            <Switch>
        <Route exact path={ROUTES.ADMIN_DETAILS} component={UserItem} />
        <Route exact path={ROUTES.ADMIN} component={UserList} />
        </Switch>
        <Footer/>
        </section>
          </div>
          </div>

   
  </div>
);

const condition = authUser =>
  authUser && !!authUser.roles[ROLES.ADMIN];

export default compose(
  withAuthorization(condition),
)(AdminPage);
